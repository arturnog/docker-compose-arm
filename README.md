##Docker-Compose for ARM

Docker Compose Official Git Repository: [here](https://github.com/docker/compose)

The official repository does not release the latest docker-compose binary for ARM architecture. Thus, requiring to manually build it.

 This repository provides the latest binary (currently version 1.24), as well as, a tutorial on how to build the latest version. This is particularly useful in order to use docker-compose on a Raspberry Pi, as the default version installed is outdated.

###Build ARM Docker-compose

1. **Clone the official repository**
    * `git clone https://github.com/docker/compose.git`
2. **Checkout the release branch**
    * `git checkout release`
3. **Prepare the Dockerfile for ARM**
    * `cp -i Dockerfile Dockerfile.armhf`
    * `sed -i -e 's/^FROM debian\:/FROM armhf\/debian:/' Dockerfile.armhf`
    * `sed -i -e 's/x86_64/armel/g' Dockerfile.armhf`
4. **Generate the executable**
    * `docker build -t docker-compose:armhf -f Dockerfile.armhf .`
    * `docker run --rm --entrypoint="script/build/linux-entrypoint"`

The executable is located in the **/dist** folder. Copy the file to a directory in an environmental variable.
    
   `sudo cp dist/docker-compose-Linux-armv7l /usr/bin/docker-compose`
    
**Note***: The building process takes at least half an hour on a Raspberry Pi 3 Model B.

###Reference

[This article](https://www.berthon.eu/2017/getting-docker-compose-on-raspberry-pi-arm-the-easy-way/) is also a reference, as it served us as a nice guide.